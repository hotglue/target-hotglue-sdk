"""{{ cookiecutter.destination_name }} target sink class, which handles writing streams."""

from __future__ import annotations

from __future__ import annotations

{%- set
    sinkclass_mapping = {
        "Per batch": "BatchSink",
        "Per record": "RecordSink",
        "SQL": "SQLSink",
    }
%}

{% set sinkclass = sinkclass_mapping[cookiecutter.serialization_method] %}

from {{ cookiecutter.library_name }}.client import {{ cookiecutter.destination_name }}Sink

class ExampleSink({{ cookiecutter.destination_name }}Sink):
    """{{ cookiecutter.destination_name }} target sink class."""

    {% if sinkclass != "SQLSink" %}
    endpoint = "/example-endpoint"
    unified_schema = NotImplementedError() # Place a unified schema class here
    name = unified_schema.schema_name
    {% endif %}

    {% if sinkclass == "RecordSink" -%}
    def process_record(self, record: dict, context: dict) -> None:
        """Process the record.

        Args:
            record: Individual record in the stream.
            context: Stream partition or context dictionary.
        """
        # Sample:
        # ------
        # client.write(record)  # noqa: ERA001

    {%- elif sinkclass == "BatchSink" -%}

    max_size = 10000  # Max records to write in one batch

    def start_batch(self, context: dict) -> None:
        """Start a batch.

        Developers may optionally add additional markers to the `context` dict,
        which is unique to this batch.

        Args:
            context: Stream partition or context dictionary.
        """
        # Sample:
        # ------
        # batch_key = context["batch_id"]
        # context["file_path"] = f"{batch_key}.csv"

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record.

        Developers may optionally read or write additional markers within the
        passed `context` dict from the current batch.

        Args:
            record: Individual record in the stream.
            context: Stream partition or context dictionary.
        """
        # Sample:
        # ------
        # with open(context["file_path"], "a") as csvfile:
        #     csvfile.write(record)

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written.

        Args:
            context: Stream partition or context dictionary.
        """
        # Sample:
        # ------
        # client.upload(context["file_path"])  # Upload file
        # Path(context["file_path"]).unlink()  # Delete local copy

    {%- elif sinkclass == "SQLSink" -%}

    connector_class = {{ cookiecutter.destination_name }}Connector
    {%- endif %}