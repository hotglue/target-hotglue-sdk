"""{{ cookiecutter.destination_name }} target class."""

from __future__ import annotations

{%- set target_class = "SQLTarget" if cookiecutter.serialization_method == "SQL" else "Target" %}

from singer_sdk import typing as th
from singer_sdk.target_base import {{ target_class }}
from target_hotglue.target import TargetHotglue

from {{ cookiecutter.library_name }}.sinks import (
    {{ cookiecutter.destination_name }}Sink,
)


class Target{{ cookiecutter.destination_name }}({{ target_class }}, TargetHotglue):
    """Sample target for {{ cookiecutter.destination_name }}."""

    name = "{{ cookiecutter.target_id }}"

    SINK_TYPES = []
    MAX_PARALLELISM = 1

    def get_sink_class(self, stream_name: str):
        return {{ cookiecutter.destination_name }}Sink


if __name__ == "__main__":
    Target{{ cookiecutter.destination_name }}.cli()
