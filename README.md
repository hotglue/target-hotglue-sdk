# target-hotglue

`target-hotglue` is a SDK for building Singer targets.

## Using our Cookiecutter template

```bash
cookiecutter git@gitlab.com:hotglue/target-hotglue-sdk.git --directory --directory="cookiecutter/target-template"
```
